# Installation
```bash
sudo apt install python3-tk python3-pip
pip install customtkinter
```
# PyInstaller
## Get customtkinter location
```bash
pip show customtkinter
```

Copy the path and add `/customtkinter:customtkinter` to it

## Create executable
```bash
python3 -m PyInstaller --onefile --add-data "/home/user/.local/lib/python3.10/site-packages/customtkinter:customtkinter" my_script.py
```

The executable is in `/dist`